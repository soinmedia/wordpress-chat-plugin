<?php get_header() ?>
<?php
$bg_img['url'] = get_bloginfo('wpurl')."/wp-content/uploads/2016/02/home-bg.jpg";
if( !empty($bg_img) ): ?>
    <div class="fullwindow" style="background: url(<?php echo $bg_img['url']; ?>); -webkit-background-size: cover; background-size: cover;background-position: center;"></div>
<?php endif; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/template.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/style.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.min.css' ?>">
<script type="text/javascript"
        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/jquery.form.min.js' ?>"></script>
<script type="text/javascript"
        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/script.js' ?>"></script>

<?php
$token_key = trim(isset($_REQUEST['token-key']) ? trim($_REQUEST['token-key']) : null);
$logged_user_id = intval($livechat->get_logged_user_id());

if (!$logged_user_id && !$token_key) {
    wp_redirect(home_url().'/login');
    exit;
}
$admin_id = $livechat->get_admin_id();
$listing_author_id = $admin_id;

if ($logged_user_id !== $admin_id && !$token_key) {
    $token_key = md5($admin_id . '-' . $logged_user_id);
    $check_session = $livechat->check_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

    if ($check_session == null)
        $livechat->add_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

    $check_session = $livechat->check_session($logged_user_id, $listing_author_id, $token_key, $admin_id);
    wp_redirect(get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $token_key);
}
if($logged_user_id === $admin_id && !$token_key){
    wp_redirect(get_permalink(get_page_by_path('live-chat')) . '?type=inbox');
}

if ($token_key):
    $check_session = $livechat->get_by_session_key($token_key);
    $user = get_user_by('id', $check_session->client_id);
    $admin = get_user_by('id', $check_session->listing_author_id);
    ?>

    <div class="live-chat-container container">
        <div class="page-cover entry-cover has-image">
            <h1 class="page-title cover-wrapper">Message</h1>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="list-group live-chat-sidebar">
                    <a href="<?php echo get_permalink(get_page_by_path('live-chat')). '?type=inbox' ?>" class="list-group-item active">
                        Inbox
                    </a>
                    <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?type=archived' ?>"
                       class="list-group-item">Archived Chats</a>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="live-chat-content panel panel-default">
                    <div class="panel-heading">
                        <h3>
                            <a href="">
                                <?php
                                    if($logged_user_id == $admin->ID)
                                        echo $user->display_name;
                                    if($logged_user_id != $admin->ID)
                                        echo $admin->display_name;
                                 ?>
                            </a>
                            <a id="live_chat_ajax_archived"
                               href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/ajax-archived.php?session-id=' . $check_session->id . '&user-id=' . $logged_user_id ?>"
                               data-return="<?php echo get_permalink(get_page_by_path('live-chat')) . '?type=archived' ?>"
                               class="btn-group btn-archive">
                                <i class="fa fa-archive"></i>
                                Archive
                                <span class="line"></span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div id="panel-body-left" class="panel-body-left">
                                    <div id="live-chat-main" class="live-chat-main">
                                        <div id="live-chat-main-box" class="live-chat-main-box">
                                            <ul class="live-chat-main-content"></ul>
                                        </div>

                                        <hr/>
                                        <form
                                            action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/send-message.php' ?>"
                                            id="live-chat-main-form" class="live-chat-main-form">
                                            <div class="row">
                                                <div class="block-message">
                                                    <div class="col-lg-10 col-md-10 col-sm-9" style="padding-right:0">
                                                        <div class="form-group">
                                                        <textarea name="message" class="form-control"
                                                                  rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="block-button">
                                                    <div class="col-lg-2 col-md-2 col-sm-3" style="padding-left:0">
                                                        <button type="submit" id="chat-button-send"
                                                                class="chat-button-send">Send
                                                        </button>
                                                        <input type="hidden" name="session_id"
                                                               value="<?php echo $check_session->id ?>"/>
                                                        <input type="hidden" name="author_id"
                                                               value="<?php echo $logged_user_id ?>"/>
                                                        <input type="hidden" name="username"
                                                               value="<?php echo ($item->author_id == $logged_user_id) ? 'You' : $livechat->get_author_name($listing_author_id) ?>"/>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div id="panel-body-right" class="panel-body-right">
                                    <div class="live-chat-listing-author">
                                        <div class="listing-author-info">
                                            <div class="chat-avatar">
                                                <?php
                                                    if($logged_user_id == $admin->ID)
                                                        $ID = $user->ID;
                                                    if($logged_user_id != $admin->ID)
                                                        $ID = $admin->ID;
                                                    echo get_avatar($ID);
                                                ?>
                                                <h5 class="space20">
                                                    <?php
                                                        if($logged_user_id == $admin->ID)
                                                            echo $user->display_name;
                                                        if($logged_user_id != $admin->ID)
                                                            echo $admin->display_name;
                                                    ?>
                                                </h5>

                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <form
        action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/ajax-message.php' ?>"
        id="live-chat-main-ajax-message">
        <input type="hidden" name="session_id"
               value="<?php echo $check_session->id ?>"/>
        <input type="hidden" name="username"
               value="<?php echo ($item->author_id == $logged_user_id) ? 'You' : $livechat->get_author_name($listing_author_id) ?>"/>

        <input type="hidden" id="livechat_latest_log_id"
               name="latest_log_id" value="0"/>
    </form>
</div>
<?php endif; ?>
<div class="clearfix"></div>
<?php get_footer() ?>
