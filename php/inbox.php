<?php
require_once("../../../../wp-config.php");

/**
 * @param $client_id
 * @param $admin_id
 * @param $current_page
 * @param $ppp
 * @return array|null|object
 */
function get_listing_inbox_by_client_id($client_id, $admin_id, $current_page, $ppp)
{
    $current_page = ($current_page - 1) * $ppp;

    global $wpdb;
    $table_name = $wpdb->prefix . "livechat_sessions";
    $tab_archived = $wpdb->prefix . "livechat_archiveds";
    $sql = "SELECT *
              FROM $table_name
            WHERE (client_id = $client_id AND listing_author_id = $admin_id)
                      AND deleted = 0
                      AND id NOT IN (SELECT session_id
                                    FROM $tab_archived
                                    WHERE user_id = $client_id AND deleted = 0)
                      LIMIT $current_page, $ppp";
    $result = $wpdb->get_results($sql);
    return $result;
}

/**
 * @param $admin_id
 * @param $current_page
 * @param $ppp
 * @return array|null|object
 */
function get_listing_inbox_by_admin_id($admin_id, $current_page, $ppp)
{
    $current_page = ($current_page - 1) * $ppp;

    global $wpdb;
    $table_name = $wpdb->prefix . "livechat_sessions";
    $tab_archived = $wpdb->prefix . "livechat_archiveds";
    $sql = "SELECT *
              FROM $table_name
            WHERE  listing_author_id = $admin_id
                      AND deleted = 0
                      AND id NOT IN (SELECT session_id
                                    FROM $tab_archived
                                    WHERE user_id = $admin_id AND deleted = 0)
                      LIMIT $current_page, $ppp";
    $result = $wpdb->get_results($sql);
    return $result;
}
/**
 * @param $session_id
 * @return array|null|object|void
 */
function get_session_log($session_id)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "livechat_logs";
    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE session_id =" . $session_id . "
                 AND deleted = 0
                 ORDER BY id DESC";
    $result = $wpdb->get_row($sql);
    return $result;
}

$logged_user_id = intval($_REQUEST['logged_user_id']);
$current_page = intval($_REQUEST['current_page']);
$ppp = intval($_REQUEST['ppp']);

/*get admin_id*/
$users_query = new WP_User_Query( array(
    'role' => 'administrator',
    'orderby' => 'display_name'
) );
$results = $users_query->get_results();
$admin_id = 0;
if($admin_id === 0){
    foreach($results as  $v){
        $admin_id = $v->ID;
    }
}
/*get admin_id*/
/**
 * is normal user
 */
if($logged_user_id !== $admin_id){
    $session_list = get_listing_inbox_by_client_id($logged_user_id, $admin_id, $current_page, $ppp);
}

/**
 * is admin
 */
if($logged_user_id === $admin_id){
    $session_list = get_listing_inbox_by_admin_id($admin_id, $current_page, $ppp);
}
/*session list cua admin va session list cua logged user*/
$end_session_list = get_listing_inbox_by_client_id($logged_user_id, $admin_id, ($current_page + 1), $ppp);
?>

<?php ob_start() ?>
<?php foreach ($session_list as $key => $item):
    $user = get_user_by('id', $item->listing_author_id);
    $client = get_user_by('id', $item->client_id);
    $latest_log = get_session_log($item->id);
    ?>
    <tr>
        <td>
            <input name="session_ids[<?php echo $item->id ?>]" value="1" type="checkbox"/>
        </td>
        <td width="5%" class="action-url" data-url="<?php echo get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $item->token_key ?>">
            <img class="user-avatar"
                 src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/img/defaultUserAvatar.png' ?>"/>
        </td>
        <td width="25%" class="action-url" data-url="<?php echo get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $item->token_key ?>">
            <span class="username">
                <?php
                    if($logged_user_id !== $admin_id){
                        echo $user->display_name ;
                    }
                    else{
                        $client = get_user_by('id', $item->author_id);
                        echo $client->display_name;
                    }
                ?>
            </span>
            <br/>
                                <span class="gray">
                                    <?php echo date('d/m/Y, h:ia', $item->date_added)?>

                                </span>
        </td>
        <td class="action-url" data-url="<?php echo get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $item->token_key ?>">
            <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $item->token_key ?>">
                <?php echo $post->post_title ?>
            </a>

            <div class="listing-description">
                <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?token-key=' . $item->token_key ?>">
                    <?php echo $latest_log && $latest_log->description != '' ? $latest_log->description : 'Send Message' ?>
                </a>
            </div>
        </td>
    </tr>
<?php endforeach; ?>

<?php if (count($session_list) == 0): ?>
    <tr>
        <td colspan="10">No record found</td>
    </tr>
<?php endif; ?>

<?php $html = ob_get_clean() ?>

<?php
echo json_encode(array(
    'status' => 'success',
    'data' => array(
        'html' => $html,
        'count_sessions' => count($end_session_list)
    )
));
?>
