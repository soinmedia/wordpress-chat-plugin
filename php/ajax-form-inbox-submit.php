<?php
require_once("../../../../wp-config.php");

$action = $_POST['action'];
$session_ids = isset($_POST['session_ids']) ? $_POST['session_ids'] : array();
$logged_user_id = intval($_POST['logged_user_id']);

foreach ($session_ids as $session_id => $value) {
    $value = intval($value);
    switch ($action) {
        case 'delete':
            $livechat->delete_session($session_id);
            break;
        case 'archive':
            $livechat->add_archived($logged_user_id, $session_id);
            break;
        case 'un-archive':
            $livechat->delete_archived($logged_user_id, $session_id);
            break;
    }
}
header("Location:" . get_permalink(get_page_by_path('live-chat')));

echo json_encode(array(
    'status' => 'success',
    'data' => array(
        'action' => $action
    )
));

