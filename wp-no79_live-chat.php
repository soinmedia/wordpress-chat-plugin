<?php
/*
Plugin Name: No79 Live Chat
Plugin URI: http://no79design.com/doc
Description: We aim to empower people so they can empower others. Our vision is to provide the platform and tools for every city in the world.
Author: Soin Media
Author URI: http://soinmedia.com
Version: 1.0
*/

if (!class_exists('Livechat_Plugin')) {
    class Livechat_Plugin
    {
        public $_name;
        public $page_title;
        public $page_name;
        public $page_id;

        public function __construct()
        {
            $this->_name = 'livechat';
            $this->page_title = 'Live Chat';
            $this->page_name = $this->_name;
            $this->page_id = '0';

            register_activation_hook(__FILE__, array($this, 'activate'));
            register_deactivation_hook(__FILE__, array($this, 'deactivate'));
            register_uninstall_hook(__FILE__, array($this, 'uninstall'));

            add_filter('page_template', array($this, 'live_chat_page_template'));

            add_action('admin_menu', 'livechat_plugin_setup_menu');
            function livechat_plugin_setup_menu()
            {
                add_menu_page('Live Chat Page', 'Live Chat Plugin', 'manage_options', 'live-chat-plugin', 'livechat_admin_init', 'dashicons-admin-comments', 30);
            }

            function livechat_admin_init()
            {
                //phpinfo(); die;
                $pagenum = isset($_REQUEST['pagenum']) ? intval($_REQUEST['pagenum']) : 1;
                $limit = 10; // number of rows in page
                $offset = ($pagenum - 1) * $limit;

                global $wpdb;
                $table_sessions = $wpdb->prefix . "livechat_sessions";
                $table_session_logs = $wpdb->prefix . "livechat_logs";
                $table_posts = $wpdb->prefix . "posts";
                $table_users = $wpdb->prefix . "users";
                $sql =
                    "SELECT ses.*, pos.post_title,
                            cli.user_login client_user_login, cli.user_nicename client_user_nicename,
                            aut.user_login author_user_login, aut.user_nicename author_user_nicename,

                            (SELECT COUNT(nolg.id)
                            FROM $table_session_logs nolg
                            WHERE nolg.session_id = ses.id) AS no_logs

                      FROM $table_sessions ses
                          LEFT JOIN $table_posts pos ON pos.id = ses.listing_id
                          LEFT JOIN $table_users cli ON cli.id = ses.client_id
                          LEFT JOIN $table_users aut ON aut.id = ses.listing_author_id

                      WHERE ses.deleted = 0

                      ORDER BY ses.last_modified DESC

                      LIMIT $offset, $limit";
                $sessions = $wpdb->get_results($sql);

                $total = $wpdb->get_var("SELECT COUNT(`id`) FROM {$wpdb->prefix}livechat_sessions");
                $num_of_pages = ceil($total / $limit);


                $page_links = paginate_links(array(
                    'base' => add_query_arg('pagenum', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;', 'text-domain'),
                    'next_text' => __('&raquo;', 'text-domain'),
                    'total' => $num_of_pages,
                    'current' => $pagenum
                ));

                require 'views/admin/index.php';
            }

            /*create admin box page*/
            add_action('admin_menu', 'register_newpage');
            function register_newpage()
            {
                //add_menu_page('Live Chat Page', 'Live Chat Plugin', 'manage_options', 'live-chat-plugin', 'livechat_admin_init', 'dashicons-admin-comments', 30);
                add_menu_page('Admin Box Chat', 'Box Chat', 'administrator', 'live_admin_box', 'adminbox');
                remove_menu_page('live_admin_box');
            }

            function adminbox()
            {
                require 'views/admin/admin-box.php';
            }


            function new_modify_user_table( $column ) {
                $column['livechat'] = 'Live Chat';
                return $column;
            }
            add_filter( 'manage_users_columns', 'new_modify_user_table' );
        }

        public function activate()
        {
            global $wpdb;

            delete_option($this->_name . '_page_title');
            add_option($this->_name . '_page_title', $this->page_title, '', 'yes');

            delete_option($this->_name . '_page_name');
            add_option($this->_name . '_page_name', $this->page_name, '', 'yes');

            delete_option($this->_name . '_page_id');
            add_option($this->_name . '_page_id', $this->page_id, '', 'yes');

            $the_page = get_page_by_title($this->page_title);

            if (!$the_page) {
                // Create post object
                $_p = array();
                $_p['post_title'] = $this->page_title;
                $_p['post_content'] = "This text may be overridden by the plugin. You shouldn't edit it.";
                $_p['post_status'] = 'publish';
                $_p['post_type'] = 'page';
                $_p['comment_status'] = 'closed';
                $_p['ping_status'] = 'closed';
                $_p['post_category'] = array(1); // the default 'Uncatrgorised'

                // Insert the post into the database
                $this->page_id = wp_insert_post($_p);
            } else {
                // the plugin may have been previously active and the page may just be trashed...
                $this->page_id = $the_page->ID;

                //make sure the page is not trashed...
                $the_page->post_status = 'publish';
                $this->page_id = wp_update_post($the_page);
            }

            delete_option($this->_name . '_page_id');
            add_option($this->_name . '_page_id', $this->page_id);

            global $wpdb;

            $charset_collate = $wpdb->get_charset_collate();

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $table_name = $wpdb->prefix . "livechat_sessions";
            $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `admin_id` int(11) NOT NULL,
                      `description` longtext COLLATE utf8_unicode_ci NOT NULL,
                      `client_id` int(11) NOT NULL,
                      `listing_author_id` int(11) NOT NULL,
                      `token_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                      `date_added` int(11) NOT NULL,
                      `last_modified` int(11) NOT NULL,
                      `author_id` int(11) NOT NULL,
                      `disabled` tinyint(4) NOT NULL DEFAULT '0',
                      `deleted` tinyint(4) NOT NULL DEFAULT '0',
                      UNIQUE KEY id (id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            dbDelta($sql);


            $table_name = $wpdb->prefix . "livechat_logs";
            $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `session_id` int(11) NOT NULL,
                      `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                      `description` longtext COLLATE utf8_unicode_ci NOT NULL,
                      `is_link` tinyint(4) NOT NULL DEFAULT '0',
                      `embed_code` longtext COLLATE utf8_unicode_ci,
                      `thumbnail` longtext COLLATE utf8_unicode_ci,
                      `author_id` int(11) NOT NULL,
                      `date_added` int(11) NOT NULL,
                      `last_modified` int(11) NOT NULL,
                      `disabled` tinyint(4) NOT NULL DEFAULT '0',
                      `deleted` tinyint(4) NOT NULL DEFAULT '0',
                      UNIQUE KEY id (id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            dbDelta($sql);


            $table_name = $wpdb->prefix . "livechat_archiveds";
            $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `session_id` int(11) NOT NULL,
                      `token_key` int(11) DEFAULT NULL,
                      `date_added` int(11) NOT NULL,
                      `last_modified` int(11) NOT NULL,
                      `disabled` tinyint(1) NOT NULL DEFAULT '0',
                      `deleted` tinyint(1) NOT NULL DEFAULT '0',
                      UNIQUE KEY id (id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
            dbDelta($sql);
        }

        public function deactivate()
        {
            $this->deletePage();
            $this->deleteOptions();
        }

        public function uninstall()
        {
            $this->deletePage(true);
            $this->deleteOptions();
        }

        private function deletePage($hard = false)
        {
            global $wpdb;

            $id = get_option($this->_name . '_page_id');
            if ($id && $hard == true)
                wp_delete_post($id, true);
            elseif ($id && $hard == false)
                wp_delete_post($id);
        }

        private function deleteOptions()
        {
            delete_option($this->_name . '_page_title');
            delete_option($this->_name . '_page_name');
            delete_option($this->_name . '_page_id');
        }

        function live_chat_page_template($page_template)
        {
            if (is_page('live-chat')) {
                $page_template = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'live-chat.php';
            }
            return $page_template;
        }

        public function generate_client_key($user_id = null)
        {
            if (!$user_id) {
                if (!empty($_SERVER['HTTP_USER_AGENT'])) {
                    $browser_info = $_SERVER['HTTP_USER_AGENT'];
                }
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                return md5($ip . $browser_info . time());
            }
            return $user_id;
        }

        function get_logged_user_id()
        {
            if (is_user_logged_in()) {
                $current_user = wp_get_current_user();
                return $current_user->ID;
            }
            return null;
        }

        function add_session($client_id, $listing_author_id, $token_key, $admin_id)
        {
            global $wpdb;
            $wpdb->insert('wp_livechat_sessions', array(
                    'admin_id' => $admin_id,
                    'client_id' => $client_id ? $client_id : 0,
                    'listing_author_id' => $listing_author_id,
                    'token_key' => $token_key,
                    'date_added' => time(),
                    'author_id' => $client_id,
                    'description' => '',
                    'disabled' => 0,
                    'deleted' => 0
                )
            );
            return true;
        }

        function add_archived($client_id, $session_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_archiveds";

            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE user_id = " . $client_id . "
                 AND session_id = " . $session_id . "
                 AND deleted = 0";
            $archived = $wpdb->get_row($sql);

            if ($archived == '') {
                $wpdb->insert('wp_livechat_archiveds', array(
                        'user_id' => $client_id,
                        'session_id' => $session_id,
                        'last_modified' => time(),
                        'date_added' => time(),
                        'disabled' => 0,
                        'deleted' => 0
                    )
                );
            }
        }

        function delete_session($session_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_sessions";
            $sql = "UPDATE $table_name
                          SET deleted = 1
                          WHERE id=$session_id";

            $wpdb->query($sql);
        }

        function delete_archived($client_id, $session_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_archiveds";

            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE user_id = " . $client_id . "
                 AND session_id = " . $session_id . "
                 AND deleted = 0";
            $archived = $wpdb->get_row($sql);

            if ($archived != '') {
                $sql = "UPDATE $table_name
                          SET deleted = 1
                          WHERE id=$archived->id";

                $wpdb->query($sql);
            }
        }

        function check_session_by_author_id($listing_author_id, $listing_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_sessions";

            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE listing_author_id=" . $listing_author_id . "
                 AND listing_id=" . $listing_id . "
                 AND deleted = 0";

            $result = $wpdb->get_row($sql);
            return $result ? $result : null;
        }

        function get_by_session_key($token_key)
        {
            global $wpdb;
            $table_name = $wpdb->prefix . "livechat_sessions";

            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE token_key = '" . $token_key . "'
                 AND deleted = 0";

            $result = $wpdb->get_row($sql);
            return $result ? $result : null;
        }


        function check_session($client_id, $listing_author_id, $token_key, $admin_id)
        {
            $check_by_client_id = $this->check_session_by_client_id($client_id, $listing_author_id, $admin_id);

            if ($check_by_client_id)
                return $check_by_client_id;

            $check_by_token_key = $this->check_session_by_token_key($token_key, $listing_author_id, $admin_id);

            if ($check_by_token_key)
                return $check_by_token_key;

            return null;
        }

        function check_session_by_client_id($client_id, $listing_author_id, $admin_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_sessions";

            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE client_id =" . $client_id . "
                 AND listing_author_id=" . $listing_author_id . "
                 AND admin_id=" . $admin_id . "
                 AND deleted = 0";

            $result = $wpdb->get_row($sql);
            return $result ? $result : null;
        }

        function check_session_by_token_key($token_key, $listing_author_id, $admin_id)
        {
            global $wpdb;

            $table_name = $wpdb->prefix . "livechat_sessions";
            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE token_key =" . $token_key . "
                 AND listing_author_id=" . $listing_author_id . "
                 AND admin_id=" . $admin_id . "
                 AND deleted = 0";

            $result = $wpdb->get_row($sql);
            return $result ? $result : null;
        }

        function get_session_log($session_id)
        {
            global $wpdb;
            $table_name = $wpdb->prefix . "livechat_logs";
            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE session_id =" . $session_id . "
                 AND deleted = 0
                 ORDER BY id DESC";
            $result = $wpdb->get_row($sql);
            return $result;
        }

        function get_session_logs($session_id)
        {
            global $wpdb;
            $table_name = $wpdb->prefix . "livechat_logs";
            $sql =
                "SELECT *
                 FROM $table_name
                 WHERE session_id =" . $session_id . "
                 AND deleted = 0";
            $result = $wpdb->get_results($sql);
            return $result;
        }

        function get_author_meta_by_id($author_id)
        {
            $author_info = get_user_meta($author_id);
            if ($author_info)
                return $author_info;
            return null;
        }

        function get_author_name($author_id)
        {
            $author_info = $this->get_author_meta_by_id($author_id);
            if ($author_info) {
                foreach ($author_info as $item) {
                    return $item[0];
                }
            }

        }

        function get_listing_inbox_by_client_id($client_id, $current_page = 1, $ppp = 10)
        {
            $current_page = ($current_page - 1) * $ppp;

            global $wpdb;
            $table_name = $wpdb->prefix . "livechat_sessions";
            $sql =
                "SELECT *
                      FROM $table_name
                      WHERE client_id =" . $client_id . "
                      AND deleted = 0
                      LIMIT $current_page, $ppp";
            $result = $wpdb->get_results($sql);
            return $result;
        }

        function get_listing_archived_by_client_id($client_id)
        {
            if (isset($client_id) && $client_id != 0) {
                global $wpdb;
                $table_name = $wpdb->prefix . "livechat_sessions";
                $table_join = $wpdb->prefix . "livechat_archiveds";
                $sql =
                    "SELECT *
                 FROM $table_name,$table_join
                 WHERE $table_name.client_id =" . $client_id . "
                 AND $table_name.id = $table_join.session_id
                 AND $table_name.deleted = 0";
                $result = $wpdb->get_results($sql);
                return $result;
            }
        }

        function count_sessions($client_id)
        {
            global $wpdb;
            $table_name = $wpdb->prefix . "livechat_sessions";
            $sql =
                "SELECT count(*) as counts
                      FROM $table_name
                      WHERE client_id =" . $client_id . "
                      AND deleted = 0";
            $result = $wpdb->get_results($sql);
            return $result;
        }

        /**
         * get_admin_id
         * @return int
         */
        function get_admin_id()
        {
            $users_query = new WP_User_Query(array(
                'role' => 'administrator',
                'orderby' => 'display_name'
            ));
            $results = $users_query->get_results();
            $admin_id = 0;
            if ($admin_id === 0) {
                foreach ($results as $v) {
                    $admin_id = $v->ID;
                }
            }
            return $admin_id;
        }


    }
}

$livechat = new Livechat_Plugin();



function new_modify_user_table_row( $val, $column_name, $user_id ) {
    global $livechat;
    $user = get_userdata( $user_id );
    switch ($column_name) {
        case 'livechat' :
            $admin_id = $livechat->get_admin_id();
            $listing_author_id = $livechat->get_admin_id();
            $logged_user_id = $user_id;
            $token_key = md5($admin_id . '-' . $logged_user_id);
            $check_session = $livechat->check_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

            if ($check_session == null)
                $livechat->add_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

            $url = get_bloginfo('wpurl') . '/wp-admin/admin.php?page=live_admin_box&token_key=' . $token_key;
            return '<a href="' . $url . '" class="button" target="_blank">Livechat</a>';
            break;
        default:
    }
    return;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );