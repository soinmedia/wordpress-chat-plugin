<?php
require_once("../../../../wp-config.php");

$session_id = intval($_GET['session-id']);
$client_id = intval($_GET['user-id']);

$livechat->add_archived($client_id, $session_id);

echo json_encode(array(
    'status' => 'success'
));