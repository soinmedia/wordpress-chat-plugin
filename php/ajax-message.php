<?php
require_once("../../../../wp-config.php");

$session_id = $_POST['session_id'];
$username = $_POST['username'];
$latest_log_id = intval($_POST['latest_log_id']);
$logged_user_id = intval($livechat->get_logged_user_id());

global $wpdb;
$tab_session_logs = $wpdb->prefix . "livechat_logs";
$tab_users = $wpdb->prefix . "users";
$sql = "SELECT lg.*, usr.display_name
        FROM $tab_session_logs lg
          LEFT JOIN $tab_users usr ON usr.ID = lg.author_id
        WHERE lg.deleted = 0
         AND lg.session_id = $session_id
                 AND lg.id > $latest_log_id
                 ORDER BY lg.id ASC";
$session_logs = $wpdb->get_results($sql);
?>

<?php ob_start() ?>
<?php foreach ($session_logs as $item):
    $latest_log_id = $item->id;
    ?>
    <li class="<?php if ($item->author_id == $logged_user_id) echo 'me'; ?>">
        <div class="chat-item chat-info">
            <table class="table table-none-boder">
                <thead>
                <tr>
                    <td width="7%"></td>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="chat-avatar">
                            <?php if ($item->author_id): ?>
                                <?php echo get_avatar($item->author_id); ?>
                            <?php else: ?>
                                <img
                                    src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/img/defaultUserAvatar.png' ?>"/>
                            <?php endif; ?>
                        </div>
                    </td>
                    <td>
                        <h5 class="chat-author">
                            <?php
                            if ($item->author_id == $logged_user_id)
                                echo 'You';
                            else
                                echo $item->display_name ?>
                        </h5>
                    </td>
                    <td>
                        <div class="chat-time">
                            <?php echo strtolower(date('d F, h:i', $item->date_added)) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <div class="chat-message">
                            <?php if ($item->is_link == 1): ?>
                                <div class="iframe-youtube">
                                    <iframe width="360" height="215"
                                            src="https://www.youtube.com/embed/<?php echo $item->embed_code ?>"
                                            frameborder="0" allowfullscreen>
                                    </iframe>
                                </div>
                            <?php elseif ($item->is_link == 2): ?>
                                <div class="iframe-youtube">
                                    <video width="360" height="215" controls>
                                        <source src="<?php echo $item->embed_code ?>" type="video/mp4">
                                        <source src="<?php echo $item->embed_code ?>" type="video/ogg">

                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            <?php elseif ($item->is_link == 0): ?>
                                <?php echo nl2br(htmlentities($item->description)); ?>

                                <?php if ($item->embed_code && $item->embed_code != ''): ?>
                                    <div class="iframe-youtube">
                                        <video width="360" height="215" controls>
                                            <source src="<?php echo $item->embed_code ?>" type="video/mp4">
                                            <source src="<?php echo $item->embed_code ?>" type="video/ogg">

                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                <?php endif; ?>

                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </li>
<?php endforeach; ?>
<?php $html = ob_get_clean() ?>


<?php
echo json_encode(array(
    'status' => 'success',
    'data' => array(
        'session_logs' => $session_logs,
        'html' => $html,
        'latest_log_id' => $latest_log_id
    )
));