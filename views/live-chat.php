<?php
$listing_id = intval(isset($_REQUEST['listing-id']) ? $_REQUEST['listing-id'] : null);
$token_key = trim(isset($_REQUEST['token-key']) ? trim($_REQUEST['token-key']) : '');
$type = trim(isset($_REQUEST['type']) ? $_REQUEST['type'] : 'box');

if($type == 'box')
    require 'box.php';

if ($type == 'archived')
    require 'archived.php';
else
    if($type == 'inbox')
        require 'inbox.php';
?>

<script>
    var plugin_url = '<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/' ?>';
    var listing_id = <?php echo $listing_id ?>;
    var token_key = '<?php echo $token_key ?>';
    var logged_user_id = <?php echo intval($livechat->get_logged_user_id()) ?>;
    var ppp = 10;
</script>
