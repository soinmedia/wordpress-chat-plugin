var enable_ajax_message = true;
var receving = 0;
var can_scroll = 1;
var div_is_scroll = 0; //as default end reached
var latest_session_log;
$(document).ready(function () {
    get_height_by_id();
    $('#livechat_latest_log_id').val(0);

    if ($('#videoField').length > 0) {
        $(":file").filestyle();
    }

    $('#live-chat-main-form [type="submit"]').click(function (e) {
        e.preventDefault();
        var form = $('#live-chat-main-form');
        /*======START CAN SCROLL=======*/
        receving = 0;
        can_scroll = 1;
        /*=======END CAN SCROLL=======*/
        var message_args = append_value();

        var urls = urlify($('[name="message"]').val());

        var params_ids = '';
        try {
            for ($i = 0; $i <= urls.length; $i++) {
                var youtube_id = get_youtube_id_from_url(urls[$i]);
                if (youtube_id != false) {
                    params_ids += '&youtube_id[]=' + youtube_id;
                }
            }
        } catch ($e) {}

        $('#live-chat-main-form [type="submit"]').addClass('hide');
        $('#send-message-loading').removeClass('hide');
        var options = {
            url: form.attr('action') + '?1' + '&is_link_youtube=' + message_args.is_link_youtube + '&embed_code=' + message_args.embed_code + params_ids,
            type: 'post',
            dataType: 'json',
            async: true,
            success: function (response) {
                latest_session_log = response.data.session_log.id;
                if (can_scroll === 1) {
                    setTimeout(function () {
                        $('#live-chat-main-box').animate({
                                    scrollTop: $('#live-chat-main-box ul').height() + 1000
                                },
                                500);

                    }, 1000);
                }
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                alert(textStatus);
                alert(error);
            },
            complete: function (response) {
                $('[name="message"]').val('');
                $('#send-message-loading').addClass('hide');
                $('#live-chat-main-form [type="submit"]').removeClass('hide');
                form[0].reset();
            }
        };

        form.ajaxForm(options);
        form.submit();

    });

    $('html').on('click', '.action-url', function () {
        var ele = $(this);
        window.location = ele.data('url');
    });


    load_ajax_message('enable');
    setTimeout(function () {
        setInterval(function () {
            load_ajax_message();
        }, 2000);
    }, 2000);

    function load_ajax_message($is_scroll) {
        if ($('.live-chat-main-content').length <= 0 || enable_ajax_message == false)
            return false;

        enable_ajax_message = false;

        check_div_is_scroll();

        var form = $('#live-chat-main-ajax-message');
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            dataType: 'json',
            async: true,
            data: form.serialize(),
            success: function (response) {
                enable_ajax_message = true;
                $('#livechat_latest_log_id').val(response.data.latest_log_id);

                if (response.data.html != "") {
                    if (div_is_scroll == 1 && receving == 1) {
                        $(".notification-box").css('display', 'block');
                    }
                    else {
                        $(".notification-box").css('display', 'none');
                    }
                    receving = 1;
                    /*======START CAN SCROLL=======*/
                    if (div_is_scroll == 1 && receving == 1) {
                        can_scroll = 0;
                    }
                    else {
                        $(".notification-box").css('display', 'none');
                        can_scroll = 1;//=> by default when message come to, auto scroll
                    }
                    /*=======END CAN SCROLL=======*/

                    $('.live-chat-main-content').append(response.data.html);
                    if (can_scroll === 1) {
                        setTimeout(function () {
                            $('#live-chat-main-box').animate({
                                        scrollTop: $('#live-chat-main-box ul').height() + 1000
                                    },
                                    500);

                        }, 1000);
                    }
                }
            },
            error: function (response) {
                enable_ajax_message = true;
            }
        });
    }

    $(".notification-box").click(function () {
        $(".notification-box").css('display', 'none');
        $('#live-chat-main-box').animate({
                    scrollTop: $('#live-chat-main-box ul').height() + 1000
                },
                500);
    });

    /**
     * @input none
     * default #live-chat-main-box
     */
    function check_div_is_scroll() {
        $('#live-chat-main-box').on('scroll', function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                div_is_scroll = 0;
            }
            else {
                div_is_scroll = 1;
            }
        })
    }


    $('#live_chat_ajax_archived').click(function (e) {
        e.preventDefault();
        var ele = $(this);
        var url = ele.attr('href');
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: '',
            success: function (response) {
                window.location = ele.data('return');
            },
            error: function (response) {
            }
        });
    });

    function load_livechat_inbox(page) {
        var url = plugin_url + 'php/inbox.php';
        $('#btn-view-more').hide();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                listing_id: listing_id,
                logged_user_id: logged_user_id,
                current_page: page,
                ppp: ppp
            },
            success: function (response) {
                $('#inbox-session-listing').append(response.data.html);
                if (parseInt(response.data.count_sessions) == 0) {
                    $('#btn-view-more').remove();
                } else {
                    $('#btn-view-more').show();
                }
            }
        });
    }

    function load_livechat_archived(page) {
        var url = plugin_url + 'php/archived.php';
        $('#btn-view-more').hide();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                listing_id: listing_id,
                logged_user_id: logged_user_id,
                current_page: page,
                ppp: ppp
            },
            success: function (response) {
                $('#archived-session-listing').append(response.data.html);
                if (parseInt(response.data.count_sessions) == 0) {
                    $('#btn-view-more').remove();
                } else {
                    $('#btn-view-more').show();
                }
            }
        });
    }

    if ($('#inbox-session-listing').length > 0) {
        var current_page = 1;
        load_livechat_inbox(current_page);
        $('#btn-view-more').click(function (e) {
            e.preventDefault();
            current_page++;
            load_livechat_inbox(current_page);
        });
    }


    if ($('#archived-session-listing').length > 0) {
        var current_page = 1;
        load_livechat_archived(current_page);
        $('#btn-view-more').click(function (e) {
            e.preventDefault();
            current_page++;
            load_livechat_archived(current_page);
        });
    }

    /*===========================START VALID MESSAGE IS LINK YOUTUBE================================*/


    /**
     * JavaScript function to match (and return) the video Id
     * of any valid Youtube Url, given as input string.
     * @author: Stephan Schmitz <eyecatchup@gmail.com>
     * @url: http://stackoverflow.com/a/10315969/624466
     */
    function ytVidId(url) {
        var find_in_text = urlify(url);
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    function is_link(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? url : false;
        /*RegExp.$1*/
    }

    /**
     * check message is_link_youtube
     * @param url
     * @returns {boolean}
     */
    function is_link_youtube(url) {
        /*
         var url_args = urlify(text); // Array url in text
         var new_text = textify(text);// String new_text without url in text*/
        if (!url && url === '') {
            return false;
        }
        else {
            return ytVidId(url);
        }
    }

    /**
     * when input change, call here to return embed link if is_link
     * get and embed message if it is_link
     * @param input_text
     * @returns {*}
     */
    function get_and_embed_message(input_text) {
        var message = $(input_text).val();
        var message_tmp = '';
        var message_args;
        /**
         * get a string, it is message in submit form
         * check this message and return current message if false
         * else if true return a string embed video
         */
        if (message.length !== 0) {
            message_tmp = is_link_youtube(message); //embed or false
        }
        else {
            message_tmp = ''; //text
        }

        // check message and message_tmp
        if (message_tmp === false) {
            return message_args = {
                embed_code: message,
                is_link_youtube: 0
            };
        }
        else {
            return message_args = {
                embed_code: message_tmp,
                is_link_youtube: 1
            }
        }
    }

    /**
     * input "message"
     * @output args [message:"youtubeID", is_link:" 1 || 0 "]
     * @returns {*}
     */
    function append_value() {
        var input_text = $('.block-message [name="message"]');

        var message_args = get_and_embed_message(input_text);
        return message_args;
    }

    /*===========================END VALID MESSAGE IS LINK YOUTUBE================================*/


});

function get_height_by_id() {
    var h = $("#panel-body-left").height();
    $('#panel-body-right').height(h + 60);
}

function show_action_list() {
    $(".list-action.action").css("display", 'block');
}
function hide_action_list() {
    $(".list-action.action").css("display", 'none');
}

/**
 * Detect url in text
 * @param text
 * @returns {*}
 */
function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    var url = text.match(urlRegex);
    return url;
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

function get_youtube_id_from_url(url) {
    if (url === undefined)
        return false;

    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match && match[7].length == 11) ? match[7] : false;
}