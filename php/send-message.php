<?php
set_time_limit(0);
$message = $_REQUEST['message'];
require_once("../../../../wp-config.php");
$author_id = $_REQUEST['author_id'];
$session_id = $_REQUEST['session_id'];
$username = $_REQUEST['username'];
$is_link_youtube = $_REQUEST['is_link_youtube'];
$embed_code = $_REQUEST['embed_code'];
$latest_session_log = 0;

function add_session_log($session_id, $author_id, $message, $is_link_youtube, $embed_code)
{
    global $wpdb;

    $uploaded_file = isset($_FILES['file']) && $_FILES['file']['tmp_name'] != '' ? $_FILES['file'] : null;
    if ($uploaded_file) {

        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
        }
        $upload_overrides = array('test_form' => false);
        $moved_file = wp_handle_upload($uploaded_file, $upload_overrides);
    }

    /*if (isset($moved_file)) {
        $message .= serialize($moved_file);
    }*/

    $log_id = $wpdb->insert('wp_livechat_logs', array(
            'session_id' => $session_id,
            'author_id' => $author_id,
            'title' => substr($message, 0, 128),
            'description' => $message,
            'is_link' => 0,
            'embed_code' => isset($moved_file) ? $moved_file['url'] : '',
            'date_added' => time(),
            'last_modified' => time(),
            'disabled' => 0,
            'deleted' => 0
        )
    );

    $youtube_ids = isset($_REQUEST['youtube_id']) ? $_REQUEST['youtube_id'] : array();
    foreach ($youtube_ids as $youtube_id) {
        $wpdb->insert('wp_livechat_logs', array(
                'session_id' => $session_id,
                'author_id' => $author_id,
                'title' => 'https://www.youtube.com/watch?v=' . $youtube_id,
                'description' => 'https://www.youtube.com/watch?v=' . $youtube_id,
                'is_link' => 1,
                'embed_code' => $youtube_id,
                'date_added' => time(),
                'last_modified' => time(),
                'disabled' => 0,
                'deleted' => 0
            )
        );
    }


    $wpdb->update('wp_livechat_sessions', array(
        'id' => $session_id,
        'last_modified' => time()
    ), array('id' => $session_id));

    $table_name = $wpdb->prefix . "livechat_logs";
    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE deleted = 0
                 AND session_id = $session_id
                 ORDER BY id DESC";
    $result = $wpdb->get_row($sql);
    return $result ? $result : null;
}

/**
 * timeSet = how long user not read message
 * @param $session_log
 * @param $timeSet
 * @return bool
 */
function is_long_time($session_log, $timeSet)
{
    $previous = find_session_log($session_log->id);
    $current_time = $session_log->date_added;
    $previous_time = $previous->date_added;
    if (!$previous_time || ($current_time >= $previous_time + $timeSet)) { //5 min
        return true;
    } else {
        return false;
    }
}

/**
 * find a session log is added befor current one item
 * @param $session_log_id
 * @return array|null|object|void
 */
function find_session_log($session_log_id)
{
    global $wpdb;

    $table_name = $wpdb->prefix . "livechat_logs";
    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE deleted = 0
                 AND id < $session_log_id
                 ORDER BY id DESC";
    $result = $wpdb->get_row($sql);
    return $result ? $result : null;
}

/**
 * @param $id
 * @return null
 */
function get_user_email($id)
{
    global $wpdb;

    $table_name = $wpdb->prefix . "users";
    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE ID = $id
                 ORDER BY id DESC";
    $result = $wpdb->get_row($sql);
    return $result ? $result->user_email : null;
}

/**
 * @param $id
 * @param $role
 * @return null
 */
function get_user_email_by_session_id($id, $role)
{
    global $wpdb;

    $table_name = $wpdb->prefix . "livechat_sessions";
    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE deleted = 0
                 AND id = $id
                 ORDER BY id DESC";
    $result = $wpdb->get_row($sql);

    if ($result && $role == 'admin') {
        return $admin_email = get_user_email($result->listing_author_id);
    } else {
        return $client_email = get_user_email($result->client_id);
    }
}

$session_log = add_session_log($session_id, $author_id, $message, 0, $embed_code);

/*==============START SEND EMAIL===============*/
$is_long_time = is_long_time($session_log, 120);

if ($is_long_time == true) {
    $admin_id = $livechat->get_admin_id();

    $admin_email = get_user_email_by_session_id($session_id, 'admin');
    $client_email = get_user_email_by_session_id($session_id, 'client');

    $subject = '[DOC Wake] New Messages';

    $message = 'Hi,<br/><br/>

    You have new messages on DOC Wake. <a href="' . get_bloginfo('url') . '/live-chat/">Click here</a> to read new messages. Or copy this link and paste to your browser ' . get_bloginfo('url') . '/live-chat/<br/>

    <br/> Best Regards,
    <br/> DOC Wake';


    //$client_email ='ntnhanbk@gmail.com';
    //$admin_email ='nhan@soinmedia.com';

    if ($author_id != $admin_id) {
        $headers = "From: " . $client_email . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        @mail($admin_email, $subject, $message, $headers);
    } else {
        $headers = "From: " . $admin_email . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        @mail($client_email, $subject, $message, $headers);
    }
} else {
}
/*===============END SEND EMAIL===============*/
?>


<?php
echo json_encode(array(
    'status' => 'success',
    'data' => array(
        'session_log' => $session_log,
        'html' => '',
        'error' => isset($moved_file) ? $moved_file  : 'empty',
        'file_name' => isset($_FILES['file']) && isset($_FILES['file']['tmp_name']) ? $_FILES['file']['tmp_name'] : ''
    )
));