<?php

function get_listing_inbox_by_admin_id($admin_id, $current_page, $ppp)
{
    $current_page = ($current_page - 1) * $ppp;

    global $wpdb;
    $table_name = $wpdb->prefix . "livechat_sessions";
    $tab_archived = $wpdb->prefix . "livechat_archiveds";
    $sql = "SELECT *
              FROM $table_name
            WHERE  listing_author_id = $admin_id
                      AND deleted = 0
                      AND id NOT IN (SELECT session_id
                                    FROM $tab_archived
                                    WHERE user_id = $admin_id AND deleted = 0)
                      LIMIT $current_page, $ppp";
    $result = $wpdb->get_results($sql);
    return $result;
}

function no_logs($session_id)
{
    global $wpdb;
    $table_logs = $wpdb->prefix . "livechat_logs";
    $sql = "SELECT COUNT(*) as count_logs
              FROM $table_logs
            WHERE  session_id = $session_id";
    $result = $wpdb->get_results($sql);
    return $result;
}

function no_logs_author($session_id)
{
    global $wpdb;
    $table_logs = $wpdb->prefix . "livechat_logs";
    $sql = "SELECT author_id
              FROM $table_logs
            WHERE id = (SELECT MAX(id)
                            FROM $table_logs
                            WHERE 1)";
    $result = $wpdb->get_results($sql);
    return $result;
}

function get_admin_id()
{
    $users_query = new WP_User_Query(array(
        'role' => 'administrator',
        'orderby' => 'display_name'
    ));
    $results = $users_query->get_results();
    $admin_id = 0;
    if ($admin_id === 0) {
        foreach ($results as $v) {
            $admin_id = $v->ID;
        }
    }
    return $admin_id;
}


$current_page = isset($_REQUEST['pagenum']) ? intval($_REQUEST['pagenum']) : 1;
$ppp = intval($_REQUEST['ppp']);
if (!$ppp) {
    $ppp = 10;
}
$admin_id = get_admin_id();
$sessions = get_listing_inbox_by_admin_id($admin_id, $current_page, $ppp);
?>

    <h1>Manage Live Chat Messages</h1>

    <table class="wp-list-table widefat fixed striped posts" width="100%">
        <thead>
        <tr>
            <td class="manage-column" width="10%">#</td>
            <td class="manage-column" width="30%">Username</td>
            <td class="manage-column" width="30%">Recipient</td>
            <td class="manage-column num" width="15%"># of messages</td>
            <td class="manage-column num column-date" width="15%">Last Activity</td>
            <td class="manage-column num column-date" width="15%">Sender</td>
        </tr>
        </thead>
        <tbody>
        <?php if (count($sessions) == 0): ?>
            <tr>
                <td colspan="20">No record found</td>
            </tr>
        <?php endif; ?>

        <?php foreach ($sessions as $key => $session): ?>
            <?php
            $user = get_user_by('id', $session->client_id);
            $admin = get_user_by('id', $session->listing_author_id);
            $no_logs = no_logs($session->id);
            $sender_id = no_logs_author($session->id);
            ?>
            <tr>
                <td class="manage-column">
                    <strong><?php echo $key + 1 ?></strong>
                </td>
                <td class="manage-column">
                    <a href="<?php echo get_edit_user_link($user->ID); ?>">
                        <strong><?php echo htmlentities($user->display_name) ?></strong>
                    </a>
                </td>
                <td class="manage-column">
                    <a href="<?php echo get_edit_user_link($admin->ID); ?>">
                        <strong><?php echo htmlentities($admin->display_name) ?></strong>
                    </a>
                </td>
                <td class="manage-column num">
                    <a href="<?php echo get_bloginfo('wpurl') . '/wp-admin/admin.php?page=live_admin_box&token_key=' . $session->token_key ?>">
                        <?php echo $no_logs[0]->count_logs; ?>
                    </a>
                </td>
                <td class="manage-column num column-date"><?php echo $session->last_modified ? date('M d, Y H:i A', $session->last_modified) : date('M d, Y H:i A', $session->date_added) ?></td>
                <td class="manage-column num column-date"><?php echo $sender_id[0]->author_id == 1 && $no_logs[0]->count_logs != 0 ? $admin->display_name : $user->display_name; ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
        <tfoot>
        <tr>
            <td class="manage-column" width="10%">#</td>
            <td class="manage-column" width="30%">Username</td>
            <td class="manage-column" width="30%">Recipient</td>
            <td class="manage-column num" width="15%"># of messages</td>
            <td class="manage-column num column-date" width="15%">Last Activity</td>
            <td class="manage-column num column-date" width="15%">Sender</td>
        </tr>
        </tfoot>
    </table>

<?php
if ($page_links) {
    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
}
?>