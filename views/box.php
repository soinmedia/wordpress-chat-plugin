<?php
$token_key = trim(isset($_REQUEST['token-key']) ? trim($_REQUEST['token-key']) : null);
$logged_user_id = intval($livechat->get_logged_user_id());

if (!$logged_user_id && !$token_key) {
    wp_redirect(home_url() . '/login');
    exit;
}
$admin_id = $livechat->get_admin_id();
$listing_author_id = $admin_id;

if ($logged_user_id !== $admin_id && !$token_key) {

    $token_key = md5($admin_id . '-' . $logged_user_id);
    $check_session = $livechat->check_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

    if ($check_session == null)
        $livechat->add_session($logged_user_id, $listing_author_id, $token_key, $admin_id);

    $check_session = $livechat->check_session($logged_user_id, $listing_author_id, $token_key, $admin_id);
    wp_redirect(get_permalink(get_page_by_path('doc/live-chat')) . '?token-key=' . $token_key);
}
if ($logged_user_id === $admin_id && !$token_key) {
    /*always redirect ADMIN to backend*/
    wp_redirect(get_bloginfo('wpurl') . '/wp-admin/admin.php?page=live_admin_box&token_key=');
    /*wp_redirect(get_permalink(get_page_by_path('live-chat')) . '?type=inbox');*/
}
?>
<?php get_header() ?>
<?php
$bg_img['url'] = get_bloginfo('wpurl') . "/wp-content/uploads/2016/02/video-main.jpg";
if (!empty($bg_img)): ?>
    <div class="fullwindow"
         style="background: url(<?php echo $bg_img['url']; ?>); -webkit-background-size: cover; background-size: cover;background-position: center;"></div>
<?php endif; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/template.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/style.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.css' ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.min.css' ?>">
<script type="text/javascript"
        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/jquery.form.min.js' ?>"></script>
<script type="text/javascript"
        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/bootstrap-filestyle-1.2.1/src/bootstrap-filestyle.min.js' ?>"></script>
<script type="text/javascript"
        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/script.js' ?>"></script>

<?php
if ($token_key):
    $check_session = $livechat->get_by_session_key($token_key);
    $user = get_user_by('id', $check_session->client_id);
    $admin = get_user_by('id', $check_session->listing_author_id);
    ?>

    <div class="live-chat-container container">
        <div class="fw-row">
            <div class="clearfix"></div>
            <div class="fw-col-xs12 fw-col-sm-6">
                <div class="block-left">
                    <div class="page-cover">
                        <h1 class="page-title cover-wrapper">DOC WAKE LIVE CHAT</h1>
                    </div>
                    <?php if ($logged_user_id === $admin_id): ?>
                        <div class="is_admin list-action" onmouseover="show_action_list()"
                             onmouseout="hide_action_list()">
                            <a href=""><i class="fa fa-list fa-2x"></i></a>

                            <div class="list-action action" onmouseout="hide_action_list()">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?type=inbox' ?>">
                                            Inbox&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?type=archived' ?>">
                                            Archived Chats
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                    <div class="live-chat-content">
                        <div class="notification-box">
                            <div class="alert alert-success">
                                You have new messages. Click here to read new messages!
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                        <div id="live-chat-main-box" class="live-chat-main-box">

                            <ul class="live-chat-main-content"></ul>
                        </div>

                        <hr/>
                        <form
                            action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/send-message.php' ?>"
                            id="live-chat-main-form" class="live-chat-main-form">
                            <table class="table table-none-boder">
                                <thead>
                                <tr>
                                    <td width="7%"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> &nbsp</td>
                                    <td colspan="2">
                                        <div class="block-message">
                                            <h3>
                                                ENTER YOUR MESSAGE BELOW:
                                            </h3>

                                            <div class="form-group">
                                                <textarea name="message" class="form-control" rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="file" accept="video/*" id="videoField"
                                                       class="filestyle" data-buttonText="Upload Video" data-size="sm"
                                                       data-buttonBefore="true"/>
                                            </div>
                                            <small>Maximum size 512MB</small>
                                            <div class="block-button">
                                                <div id="send-message-loading" class="hide">
                                                    <img
                                                        src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/img/ajax-loader.gif' ?>"/>
                                                    Sending ...
                                                </div>
                                                <button type="submit" id="chat-button-send"
                                                        class="chat-button-send">
                                                    SUBMIT
                                                </button>
                                                <input type="hidden" name="session_id"
                                                       value="<?php echo $check_session->id ?>"/>
                                                <input type="hidden" name="author_id"
                                                       value="<?php echo $logged_user_id ?>"/>
                                                <input type="hidden" name="username"
                                                       value="<?php echo ($item->author_id == $logged_user_id) ? 'You' : $livechat->get_author_name($listing_author_id) ?>"/>
                                            </div>
                                    </td>

                                </tr>
                                </tbody>
                            </table>

                            <div class="clearfix"></div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <form
                        action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/ajax-message.php' ?>"
                        id="live-chat-main-ajax-message">
                        <input type="hidden" name="session_id"
                               value="<?php echo $check_session->id ?>"/>
                        <input type="hidden" name="username"
                               value="<?php echo ($item->author_id == $logged_user_id) ? 'You' : $livechat->get_author_name($listing_author_id) ?>"/>

                        <input type="hidden" id="livechat_latest_log_id"
                               name="latest_log_id" value="0"/>
                    </form>
                </div>
            </div>
            <div class="fw-col-xs12 fw-col-sm-6">
                <div class="block-right">
                    <?php $page = get_page_by_path('live-chat-right-content'); ?>
                    <div class="block-right-content">
                        <p><?php echo $page->post_content ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

<?php endif; ?>
<?php get_footer() ?>
