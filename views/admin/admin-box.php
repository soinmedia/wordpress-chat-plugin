<?php
$token_key = trim(isset($_REQUEST['token_key']) ? trim($_REQUEST['token_key']) : null);
if (!$token_key) {
    wp_redirect(get_bloginfo('wpurl') . '/wp-admin/admin.php?page=live-chat-plugin');
    exit;
}
?>

<?php
/*Function*/
/**
 * get_session_key_by Token key
 * @param $token_key
 * @return array|null|object|void
 */
function get_by_session_key($token_key)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "livechat_sessions";

    $sql =
        "SELECT *
                 FROM $table_name
                 WHERE token_key = '" . $token_key . "'
                 AND deleted = 0";

    $result = $wpdb->get_row($sql);
    return $result ? $result : null;
}

/**
 * @param $session_id
 * @param $latest_log_id
 * @return array|null|object
 */
function get_session_logs($session_id, $latest_log_id)
{
    global $wpdb;
    $tab_session_logs = $wpdb->prefix . "livechat_logs";
    $tab_users = $wpdb->prefix . "users";
    $sql = "SELECT lg.*, usr.display_name
        FROM $tab_session_logs lg
          LEFT JOIN $tab_users usr ON usr.ID = lg.author_id
        WHERE lg.deleted = 0
         AND lg.session_id = $session_id
                 AND lg.id > $latest_log_id
                 ORDER BY lg.id ASC";
    return $session_logs = $wpdb->get_results($sql);
}

/**
 * @return int|null
 */
function get_logged_user_id()
{
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        return $current_user->ID;
    }
    return null;
}

?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/template.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/admin-css.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.min.css' ?>">
    <script type="text/javascript"
            src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/jquery.form.min.js' ?>"></script>
    <script type="text/javascript"
            src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/bootstrap-filestyle-1.2.1/src/bootstrap-filestyle.min.js' ?>"></script>
    <script type="text/javascript"
            src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/script.js' ?>"></script>
<?php
if ($token_key):
    $check_session = get_by_session_key($token_key);
    $user = get_user_by('id', $check_session->client_id);
    $admin = get_user_by('id', $check_session->listing_author_id);
    $logged_user_id = intval(get_logged_user_id());

    $latest_log_id = intval($_POST['latest_log_id']);
    $session_logs = get_session_logs($check_session->id, $latest_log_id);
    ?>
    <div class="container">
        <div class="page-header">
            <a href="<?php echo get_bloginfo('wpurl') . '/wp-admin/admin.php?page=live-chat-plugin' ?>"
               class="header-archived-btn"><i class="fa fa-angle-left"></i> Back</a>
        </div>
        <div class="box-chat">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="box-chat-header">
                        <div class="header-title text-center">
                            <?php echo $user->display_name;  ?> - <em><?php echo $user->user_email; ?></em>
                        </div>
                    </div>
                </div>
                <div class="panel panel-body">
                    <div class="box-chat-left">
                        <div class="left-item">
                            <div class="live-chat-content">
                                <div class="notification-box">
                                    <div class="alert alert-success">
                                        You have new messages. Click here to read new messages!
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                                <div id="live-chat-main-box" class="live-chat-main-box">
                                    <ul class="live-chat-main-content"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="left-form">
                            <form
                                action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/send-message.php' ?>"
                                id="live-chat-main-form" class="live-chat-main-form">
                                <table class="table table-none-border">
                                    <tbody>
                                    <tr>
                                        <td width="7%"></td>
                                        <td colspan="2">
                                            <div class="form-group block-message">
                                                <textarea name="message" class="form-control" rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="file" accept="video/*" id="videoField"
                                                       class="filestyle" data-buttonText="Upload Video" data-size="sm"
                                                       data-buttonBefore="true"/>
                                                <small>Maximum size 512MB</small>
                                            </div>
                                            <div class="block-button">
                                                <div id="send-message-loading" class="hide">
                                                    <img src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/img/ajax-loader.gif' ?>" /> Sending ...
                                                </div>
                                                <button type="submit" id="chat-button-send"
                                                        class="chat-button-send">
                                                    SUBMIT
                                                </button>
                                                </button>
                                                <input type="hidden" name="session_id" value="<?php echo $check_session->id ?>">
                                                <input type="hidden" name="author_id" value="<?php echo $admin->ID ?>">
                                                <input type="hidden" name="username" value="<?php echo $admin->user_login ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>

                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <form
            action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/ajax-message.php' ?>"
            id="live-chat-main-ajax-message">
            <input type="hidden" name="session_id"
                   value="<?php echo $check_session->id ?>"/>
            <input type="hidden" name="username"
                   value="<?php echo $admin->user_login  ?>"/>

            <input type="hidden" id="livechat_latest_log_id"
                   name="latest_log_id" value="0"/>
        </form>
    </div>
    <div class="clearfix"></div>

<?php endif; ?>