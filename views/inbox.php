<?php get_header() ?>
<?php
$bg_img['url'] = get_bloginfo('wpurl')."/wp-content/uploads/2016/02/home-bg.jpg";
if( !empty($bg_img) ): ?>
    <div class="fullwindow" style="background: url(<?php echo $bg_img['url']; ?>); -webkit-background-size: cover; background-size: cover;background-position: center;"></div>
<?php endif; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/template.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/style.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.css' ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/css/font-awesome-4.5.0/css/font-awesome.min.css' ?>">
    <script type="text/javascript"
            src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/jquery.form.min.js' ?>"></script>
    <script type="text/javascript"
            src="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/js/script.js' ?>"></script>


<?php
$logged_user_id = intval($livechat->get_logged_user_id());
if (!$logged_user_id) {
    wp_redirect(home_url() . '/login');
    exit;
}
?>
    <div class="live-chat-container full-width ">
        <div class="page-cover entry-cover has-image">
            <h1 class="page-title cover-wrapper">Inbox Chats</h1>
        </div>
        <form id="form_inbox"
              action="<?php echo get_bloginfo('wpurl') . '/wp-content/plugins/no79_live-chat/php/ajax-form-inbox-submit.php' ?>"
              method="post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="list-group live-chat-sidebar">
                        <a href="<?php echo get_permalink(get_page_by_path('live-chat')). '?type=inbox' ?>"
                           class="list-group-item active">
                            Inbox
                        </a>
                        <a href="<?php echo get_permalink(get_page_by_path('live-chat')) . '?type=archived' ?>"
                           class="list-group-item">Archived Chats</a>
                    </div>
                </div>
                <div class="col-sm-9">

                    <table class="table table-chat">
                        <thead>
                        <tr>
                            <th width="5%">
                                <input type="checkbox"/>
                            </th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th class="middle-block">
                                <button type="submit" name="action" value="archive" class="btn-white">Archive</button>
                                <button type="submit" name="action" value="delete" class="btn-white">Delete</button>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="inbox-session-listing">

                        </tbody>
                    </table>

                    <a id="btn-view-more" class="btn-white btn-block">Load More</a>

                </div>
            </div>
            <input type="hidden" name="logged_user_id" value="<?php echo $logged_user_id ?>"/>
        </form>
        <div class="clearfix"></div>
    </div>
</div>
<?php get_footer() ?>